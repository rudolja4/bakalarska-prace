# BI-BAP
Detecting abnormalities in X-Ray images using Neural Networks

Jan Rudolf - Bachelor thesis

Use conda environment specified in ```environment.yml```. Use jupyter notebook to browse. 

### Data

Models and MURA dataset can be downloaded from:
https://drive.google.com/drive/folders/17v6MCQeWuI67ihUSrC573xAsOvtUh0QQ?usp=sharing

### Structure
```
project
|   README.md
|   environment.yml # Contains conda env with all necessary packages for any script
|   environment_basic.yml # Contains conda env with packages needed for tuning and training
|---src # contains scripts and JNs
|   |   baseline.ipynb # Notebok for creation of the baseline model
|   |   check_model.ipynb # Model browsing notebook
|   |   compare_models.ipynb # Notebok for comparison of the baseline model
|   |   densenet.ipynb # Notebok for creation of a densenet model
|   |   ensemble_model.ipynb # Notebook for creation and training ensemble model
|   |   hp_search_overview.ipynb # Displays results of hp search for resnet
|   |   resnet.ipynb # Notebok for creation of resnet models
|   |   test_split.ipynb # Scrip used for splitting the training dataset into new test and train
|   |   padding_generator.py # padding generator and augmentation definitions
|   |   res_gen.py # generator for res blocks
|   |   reset.py # cleans model project folder of the first param, usage python3 reset.py model_name
|   |   resnet_hm.py # hyper model for hp search
|   |   resnet_hp.py # resnet hp search
|   |   tf_utils.py # provides utils for tf and 
|   |   train.py # script for model training, usage python3 train.py model_name <args>
|   |   utils.py # model utils
|   |   visual_utils.py # grad cam and image utils
|   |   weight_hp.py # search for threshold parameter
|---MURA-v1.1 # contains dataset. Is not part of git repository. 
|   |   test_studies.csv # defines test split
|   |   
|   |   ... dataset files
|   |   
|---models # folder where all models are saved
```

### General arguments
Arguments supported by majority of scrpits (and notebooks)

```--cpu``` forces usage of CPU

```--single-gpu``` forces usage of single GPU. Also ensures that selected GPU is not fully alocated. Works as ```--cpu``` if no such gpu is available. 

```--memory x``` limits allocation of GPU memory to ```x``` MB. It is recommended to use this option together with ```--single-gpu``` since the flag limits ONLY first found GPU

```--dataset-folder``` sets folder of dataset. Default: ```../MURA-v1.1```

```--model-folder``` sets folder of outputs. Default: ```../models```

### Training
Training is done using train.py. Usage: ```python3 train.py model_name <args>```

Saves all outputs into ```MODEL_FOLDER/model_<model_name>```. 

| Parameter                                  | Effect                                                            | Default                               |
|--------------------------------------------|-------------------------------------------------------------------|---------------------------------------|
| ```--batch-size <batch_size>```            | sets batch size                                                   | 64                                    |
| ```--class-weights```                      | applies class weights                                             | -                                     |
| ```--decay <decay_rate, decay_interval>``` | sets decay                                                        | -                                     |
| ```--epochs <epochs_limit>```              | sets epoch limit                                                  | 100000                                |
| ```--es-patience <patience>```             | early stopping patience                                           | 30                                    |
| ```--es-min_delta <delta>```               | early stopping minimal change                                     | 0.01                                  |
| ```--gen <gen_code>```                     | sets augmentation for training                                    | b                                     |
| ```--vgen <gen_code>```                    | sets augmentation for validation set                              | b                                     |
| ```--init <model_path>```                  | sets path to the trained model                                    | ```model_folder/model_<model_name>``` |
| ```--lrs```                                | applies adaptive learning rate schedule                           | -                                     |
| ```--mirror```                             | Use multiple gpus. (Model must be compiled within mirror context) | -                                     |

#### Augmentation codes
Generator codes: 

b - no augmentation

1 - Modest aug

2 - Strong aug

suffix e for equalization, eg: 1e

suffix g for gauss noise, eg: 2g

### HP tuning
Tuning HPs can be done by running script on single device or by running 1 chief - many workers. Chief - workers approach runs each trail on different device.

#### In order to run chief:
It is recommended to run with ```--cpu``` option to save computational capacity of GPUs for workers.

```
export KERASTUNER_TUNER_ID="chief"
export KERASTUNER_ORACLE_IP="127.0.0.1"
export KERASTUNER_ORACLE_PORT="8000"
 
python3 resnet_hp.py --cpu
```

#### In order to run worker:
Every single worker needs an unique ID.
```
export KERASTUNER_TUNER_ID="tuner{ID}"
export KERASTUNER_ORACLE_IP="127.0.0.1"
export KERASTUNER_ORACLE_PORT="8000"

python3 resnet_hp.py --single-gpu --memory 8000
```